const { ApolloServer, gql } = require("apollo-server");
const { buildFederatedSchema } = require("@apollo/federation");
const { PrismaClient } = require('@prisma/client')

const prisma = new PrismaClient()

const typeDefs = gql`
  scalar DateTime
  
  type Lead {
      id: Int!
      payload_id: String!
      account_id: String!
      first_name: String!
      middle_name: String!
      last_name: String!
      phone: String!
      region_group_id: Int!
      email: String!
      source_entity_id: String!
      owner_entity_id: String!
      stage_id: Int!
      remax_boundary_key: String
      created_at: String!
      updated_at: String!
      deleted_at: String!
  }
  
  type Query {
      leads(
        email: String
        source_entity_id: Int
        first_name: String
        last_name: String
        phone: String
        remax_boundary_key: String
        stage_id: Int
        
      ): [Lead!]!
  }
`;

const resolvers = {
    Query: {
        leads: async () => {
            const leads = prisma.leads.findMany()
            for (const key in leads) {
                console.log(`${key}: ${leads[key]}`)
            }
            return await prisma.leads.findMany()
        }
    }
};

const start = async () => {
    const server = new ApolloServer({
        schema: buildFederatedSchema({
            typeDefs,
            resolvers,
        })
    });
    server.listen(process.env.PORT).then(({ url }) => {
        console.log(`LAPI running at: ${url}`);
    });
};

start();
